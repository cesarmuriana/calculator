const calculator = require('./calculator');
const errores = require('./error');

test('sum 1 to 9 all in middle is 45', () => {
    expect(calculator([1,2,3,4,5,6,7,8,9],'sum')).toBe(45);
});

test('sum 10 and 30 and 50 is 90', () => {
    expect(calculator([10,30,50],'sum')).toBe(90);
});

test('sum 0 and 0 is 0', () => {
    expect(calculator([0,0],'sum')).toBe(0);
});

test('array is empty = 0', () => {
    expect(calculator([],'sum')).toBe("El array esta vacio");
});

test('mult 2 x 6 is 12', () => {
    expect(calculator([2,6],'mult')).toBe(12);
});

test('double 6 is 12', () => {
    expect(calculator([6],'double')).toBe[12];
});

test('Array is doubled', () => {
    expect(calculator([6,2,3,4,5],'double')).toBe[12,4,6,8,10];
});
/*
test('La operacion no existe', () => {
    expect(calculator([6,4],'adssad')).toThrow('errores');
});*/