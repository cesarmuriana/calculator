const errores = require('./error');
const persona = require('./persona');

/* Se declaran y programan las funcionalidades de la calculadora */

    /* Suma */

let fSum = (a) => {
            if (a.length===0) return errores(0);
            return a.reduce((valAnt,valAct) => {return valAnt+valAct})
};

    /* Doble */

let fDouble = (a) => {return a.reduce((valAct) => {return valAct*2})}

    /* Multiplicar */

let fMult = (a) => {let valAnt=1; return a.reduce((valAnt,valAct) => {return valAnt*valAct})}

/* Si la persona no es un admin o un usuario se le denega el acceso a calcular */

let calc = (a,b,m) => { if(m === "admin" || m === "user"){
                            try{ 
                                return operationObnect[b](a)
                            }catch(err){
                                return errores(2)
                            } 
                        }else{
                            return errores(3);
                        }
                    };

let operationObnect={
    'sum':fSum,
    'double':fDouble,
    'mult':fMult
};

/* Se declara 'me' como una persona */

let me = new persona("cesar","muriana","cesarmuriana@gmail.com","admin");

//name, surname, email, roll
//module.exports = calc;

/* Se recoje me.roll para saber que tipo de persona es*/

console.log(calc([100,10,20,30,50,60],'sum',me.roll));