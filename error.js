let errores = {
    '0':'El array esta vacio',
    '1':'Ha fallado la suma',
    '2':'La operacion no existe',
    '3':'Error, este usuario no tiene permitido el acceso'
}

let back = (a) => {return errores[a]};


module.exports = back;